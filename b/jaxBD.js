$('#Login').on('click', function (){
    event.preventDefault();
        $.ajax({
        url: 'LoginBD.php',
        type: 'POST',
        dataType: 'json',
        data: {
            "email": $('#Username').val(),
            "psw": $('#Password').val()
        },
        success: function(data) {
            response(data);
        },
        error: function(jqXHR, textStatus, error) {
            console.log(jqXHR);
            alert("Error:" + textStatus+ " "+ error);
        }
    });
})
$('#Register').on('click', function (){
    event.preventDefault();
    $.ajax({
        url: 'RegisterBD.php',
        type: 'POST',
        dataType: 'json',
        data: {
            "email": $('#Username').val(),
            "psw": $('#Password').val()
        },
        success: function(data) {
            console.log(data);
            response(data);
            window.stop();
        },
        error: function(jqXHR, textStatus, error) {
            console.log(jqXHR);
            //alert("Error:" + textStatus+ " "+ error);
        }
    });
})

$('#GetUser').on('click', function (){
    event.preventDefault();
    $.ajax({
        url: 'getUsersBD.php',
        type: 'POST',
        dataType: 'json',
        data: {
            "email": $('#Username').val(),
            "psw": $('#Password').val()
        },
        success: function(data) {
            $('#cosa').html('');
            for (let i = 0; i<data.length; i++){
                let us = "<p>" + data[i].email+"</p>";
                $('#cosa').append(us);
            }
        },
        error: function(jqXHR, textStatus, error) {
            console.log(jqXHR);
            alert("Error:" + textStatus+ " "+ error);
        }
    });
})

function response(data) {
    console.log(data);

    if (data === 'OK') {
        $('#cosa').css('color', 'green').text('Usuari: ' + data.usuari_app);
    } else {
        $('#cosa').css('color', 'red').text(data.error + ': ' + data.usuari_app);
    }
}