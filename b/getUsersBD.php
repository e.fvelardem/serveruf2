<?php

require_once(UsuariIContrasenya.php);

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM usuaris_app");
    $query->execute();


    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    print_r(json_encode($result));

} catch(PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
