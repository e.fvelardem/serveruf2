<?php

require_once(UsuariIContrasenya.php);

$email = $_POST['email'];
$credencial = $_POST['psw'];
$institutDomain = "ies-sabadell.cat";

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $conn->prepare("SELECT * FROM usuaris_app WHERE nom = :email AND password = :credencial");
    $query->bindParam(":email", $email, PDO::PARAM_STR);
    $query->bindParam(":credencial", $credencial, PDO::PARAM_STR);
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    if ($query->rowCount() >= 1) {
        $response = array(
            'estat' => 'OK',
            'error' => '',
            'correu' => $email,
            'credencial' => $credencial
        );
        echo json_encode($response);
    } else {
        $query2 = $conn->prepare("SELECT * FROM usuaris_app WHERE nom = :email");
        $query2->bindParam(":email", $email, PDO::PARAM_STR);
        $query2->execute();
        $result2 = $query2->fetch(PDO::FETCH_ASSOC);

        if ($query2->rowCount() >= 1) {
            $response = array(
                'estat' => 'KO',
                'error' => 'Credencial Incorrecte',
                'correu' => $email,
                'credencial' => $credencial
            );
            echo json_encode($response);
        } else {
            $response = array(
                'estat' => 'KO',
                'error' => 'Correu Incorrecte',
                'correu' => $email,
                'credencial' => $credencial
            );
            echo json_encode($response);
        }
    }
} catch (PDOException $e) {
    $response = array(
        'estat' => 'KO',
        'error' => 'Error de connexió: ' . $e->getMessage(),
        'correu' => $email,
        'credencial' => $credencial
    );
    echo json_encode($response);
}
?>
