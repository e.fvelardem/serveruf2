<?php

require_once(UsuariIContrasenya.php);

$email = $_POST['email'];
$credencial = $_POST['psw'];
$institutDomain = "ies-sabadell.cat";

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Configura llançament d'excepcions en cas d'error.

    $query = $conn->prepare("SELECT * FROM usuaris_app WHERE nom = :email");
    $query->bindParam(":email", $email);
    $query->execute();

    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    if ($query->rowCount() >= 1) {
        $response = array(
            'estat' => 'KO',
            'error' => 'L\'usuari ja existeix',
            'usuari_app' => $email
        );
        print_r(json_encode($response));
    } else {
        // Verifica el domini de l'institut
        if (!str_ends_with($email, $institutDomain)) {
            $response = array(
                'estat' => 'KO',
                'error' => 'Correu incorrecte',
                'usuari_app' => $email
            );
            print_r(json_encode($response));
        } else {
            // Insereix el nou usuari
            $query = $conn->prepare("INSERT INTO usuaris_app (nom, password) VALUES (:email, :psw)");
            $query->bindParam(":email", $email);
            $query->bindParam(":psw", $credencial);
            $query->execute();

            $response = array(
                'estat' => 'OK',
                'error' => '',
                'usuari_app' => $email
            );
            print_r(json_encode($response));
        }
    }
} catch (PDOException $e) {
    $response = array(
        'estat' => 'KO',
        'error' => 'Error de connexió: ' . $e->getMessage(),
        'usuari_app' => $email
    );
    print_r(json_encode($response));
}
?>










