<?php

$nomCancion = $_POST['buscar'];

require_once("UsuariIContrasenya.php")

try {

    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);

    $userQuery = $conn->prepare("SELECT * FROM songs WHERE Nom = :cancion");
    $userQuery->bindParam("cancion", $cancion);
    $userQuery->execute();
    $userData = $userQuery->fetch(PDO::FETCH_ASSOC);

    if ($userQuery -> rowCount() >= 1){
        print_r(json_encode("Ja existeix aquesta cançó"));
    }
    else{
        $insertQuery = $conn->prepare("INSERT INTO songs (Nom, Reproduccions, Recaptacio) VALUES (:nomCancion, 1000, 100)");
        $insertQuery->bindParam("nomCancion", $nomCancion);
        $insertQuery->execute();
        print_r(json_encode("Cançó afegida amb éxit"));
    }


} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
