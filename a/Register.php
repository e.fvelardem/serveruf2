<?php

$correu = $_POST['correu'];
$credencial= $_POST['credencial'];

require_once("UsuariIContrasenya.php")

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM usuaris_app where nom = :correu");
    $query->bindParam("correu", $correu, PDO::PARAM_STR);
    $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);

    if($query->rowCount() >= 1){
        $response = array(
            'estat' => 'KO',
            'error' => "L'usuari ja existeix",
            'usuari_app' => $correu
        );
        print_r(json_encode($response));
    }
    else if($query->rowCount() >= 1){
        print_r(json_encode(array("Estado" => "KO","error" => "Credencial incorrecta","usuari_app"=>$correu)));
        exit();
    }else if(str_contains($correu, '@ies-sabadell.cat')){

        $query = $conn->prepare("insert into usuaris_app(nom, password) values (:correu, :credencial)");
        $query->bindParam("correu", $correu, PDO::PARAM_STR);
        $query->bindParam("credencial", $credencial, PDO::PARAM_STR);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        print_r(json_encode(array("Estado" => "OK","error" => " ","usuari_app"=>$correu)));
        exit();
    }
    //print_r(json_encode(array("Estado" => "KO","error" => "Correu incorrecte","usuari_app"=>$correu)));

}catch(PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());

}
