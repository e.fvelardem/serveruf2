$("#search").on("click",function(){
    event.preventDefault();
    let buscar = $("#buscar").val().trim();
    if (buscar === ""){
        $.ajax({
            method:"GET",
            url:"song.php",
            dataType:"json",

            success:function (data){
                console.log(data);
                $('#head').empty().append('<tr><th>ID</th><th>NOM</th><th>RECAPTACIO</th></tr>')
                $('#body').empty()
                $.each(data,function (index,song){
                    $('#body').append('<tr><td>' + song.Id + '</td><td>' + song.Nom + '</td><td>' + song.Recaptacio +  '</td></tr>');
                });
                window.stop();
            },
            error:function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);

            }
        })
    }
    else {
        $.ajax({
            method:"POST",
            url:"buscar.php",
            data:{"buscar": $("#buscar").val()},
            dataType:"json",

            success:function (data){
                console.log(data);
                $('#head').empty().append('<tr><th>ID</th><th>NOM</th><th>REPRODUCCIONS</th></tr>')
                $('#body').empty()
                $.each(data,function (index,song){
                    $('#body').append('<tr><td>' + song.Id + '</td><td>' + song.Nom + '</td><td>' + song.Reproduccions +  '</td></tr>');
                });
                window.stop();
            },
            error:function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);

            }
        })
    }

});