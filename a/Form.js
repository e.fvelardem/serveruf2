var foto = document.getElementById("imageform")

foto.style.width = "40%";
foto.style.height = "40%";

let novull = document.getElementById("cancel")
novull.addEventListener("click", function(){
    document.getElementById("text").innerHTML = "L'usuari ha cancel·lat la inscripció"
})
//exampleInputEmail1 --> Email Address

//exampleInputPassword1 --> Password

$("#imageform").mouseenter(function (event){
    $("#imageform").animate({
        width: "45%",
        height: "45%"
    }, 500)
});
$("#imageform").mouseleave(function (event){
    $("#imageform").animate({
        width: "40%",
        height: "40%"
    }, 500)
});
$("#register").on("click",function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"Register.php",
        data:{"correu": $("#Email").val(), "credencial": $("#password").val()},
        dataType:"json",

        success:function (data){
            console.log(data);

            if (data.Estado === "KO"){
                document.body.innerHTML += "<p style='color: red'>" + "error: " + data.error + " Usuario: " + data.usuari_app + "</p>";
            }
            else{
                document.body.innerHTML += "<p style='color: green'>" + "Success: " + data.error + " Usuario: " + data.usuari_app + "</p>";
            }
            window.stop();
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);

        }
    })
});
if (Object.keys(localStorage).length !== 0){
    $("#logout").on("click",function(){
        event.preventDefault();
        localStorage.clear();
        $('#result').html("<p style='color: green'>" + "S'ha sortit de la sesió correctament' " + "</p>" +
            "<p>" + " El text guardat és " + localStorage.getItem('correu') + "</p>");
    });
}
else {
    $("#login").on("click",function(){
        event.preventDefault();
        $.ajax({
            method:"POST",
            url:"Login.php",
            data:{"correu": $("#Email").val(), "credencial": $("#password").val()},
            dataType:"json",

            success:function (data){
                console.log(data);
                if (data.Estado === "KO"){
                    //document.body.innerHTML = "<p style='color: red'>" + "error: " + data.error + " Usuario: " + data.usuari_app + "</p>";
                    $('#result').html("<p style='color: red'>" + "error: " + data.error + " Usuario: " + data.usuari_app + "</p>");
                }
                else {
                    localStorage.setItem('correu', $("#Email").val());
                    //document.body.innerHTML = "<p style='color: green'>" + "error: " + data.error + " Usuario: " + data.usuari_app + "</p>";
                    $('#result').html("<p style='color: green'>" + "Success: " + data.error + " Usuario: " + data.usuari_app + "</p>" +
                        "<p>" + " El text guardat és " + localStorage.getItem('correu') + "</p>");
                    // document.getElementById("#borrar").style.display="block";

                    //document.body.innerHTML = "El text guardat és " + localStorage.getItem('paraula');


                }
                window.stop();
            },
            error:function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);

            }
        })
    });
}