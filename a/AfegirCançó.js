$("#cancion").on("click",function(){
    event.preventDefault();
    let buscar = $("#buscar").val().trim();
    if (buscar === ""){
        document.body.innerHTML += "<p> No s'admeten espais en blanc";
    }
    else{
        $.ajax({
            method:"POST",
            url:"añadirCancion.php",
            data:{"buscar": $("#buscar").val()},
            dataType:"json",

            success:function (data){
                console.log(data);

                document.body.innerHTML += "<p>" + data;

                window.stop();
            },
            error:function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);

            }
        })
    }

});