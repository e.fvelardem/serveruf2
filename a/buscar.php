<?php

$cancion = $_POST['buscar'];

require_once("UsuariIContrasenya.php")

try{
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);

    $userQuery = $conn->prepare("SELECT * FROM songs WHERE Nom = :cancion");
    $userQuery->bindParam("cancion", $cancion);
    $userQuery->execute();
    $userData = $userQuery->fetch(PDO::FETCH_ASSOC);

    $array =[array("Id" => $userData["idSongs"],
        "Nom" => $userData["Nom"],
        "Reproduccions" => $userData["Reproduccions"],
        "Recaptacio" => $userData["Recaptacio"])];

    print_r(json_encode($array));
} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
