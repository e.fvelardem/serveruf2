<?php

$UserId = getUserId();
$SongId = getSongId();

require_once("UsuariIContrasenya.php")

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);

    $userQuery = $conn->prepare("SELECT nom FROM usuaris_app WHERE id = :UserId");
    $userQuery->bindParam("UserId", $UserId, PDO::PARAM_INT);
    $userQuery->execute();
    $userData = $userQuery->fetch(PDO::FETCH_ASSOC);
    $username = $userData['nom'];

    $songQuery = $conn->prepare("SELECT Nom FROM songs WHERE idSongs = :SongId");
    $songQuery->bindParam("SongId", $SongId, PDO::PARAM_INT);
    $songQuery->execute();
    $songData = $songQuery->fetch(PDO::FETCH_ASSOC);
    $songName = $songData['Nom'];

    $query = $conn->prepare("SELECT * FROM usuaris_songs WHERE idusuari=:UserId");
    $query->bindParam("UserId", $UserId, PDO::PARAM_STR);
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    if ($query->rowCount() >= 1) {
        foreach ($result as $row) {
            $songData = json_decode($row['reproduccions'], true);
            $existe = false;
            foreach ($songData as $song) {
                if ($song['song'] == $SongId) {
                    $song['reproduccions'] = $song['reproduccions']+1;
                    $existe = true;
                    break;
                }
            }
            if (!$existe) {
                $songData[] = array('song' => $SongId, 'reproduccions' => 1);
            }
        }
        $reproduccions = json_encode($songData);
        $updateQuery = $conn->prepare("UPDATE usuaris_songs SET reproduccions = :reproduccions WHERE idusuari = :UserId");
        $updateQuery->bindParam("reproduccions", $reproduccions, PDO::PARAM_STR);
        $updateQuery->bindParam("UserId", $UserId, PDO::PARAM_INT);
        $updateQuery->execute();

        $updateSongQuery = $conn->prepare("UPDATE songs SET Reproduccions = Reproduccions + 1, Recaptacio = Recaptacio + 2 WHERE idSongs = :SongId");
        $updateSongQuery->bindParam("SongId", $SongId, PDO::PARAM_INT);
        $updateSongQuery->execute();

        $a = "L'usuari (" . $username . ") ha incrementat les reproduccions de la cançó (" . $songName . ").";
    } else {
        $data = array(array('song' => $SongId, 'reproduccions' => 1));
        $reproduccions = json_encode($data);
        $insertQuery = $conn->prepare("INSERT INTO usuaris_songs (idusuari, reproduccions) VALUES (:UserId, :reproduccions)");
        $insertQuery->bindParam("UserId", $UserId, PDO::PARAM_STR);
        $insertQuery->bindParam("reproduccions", $reproduccions, PDO::PARAM_STR);
        $insertQuery->execute();

        $updateSongQuery = $conn->prepare("UPDATE songs SET Reproduccions = Reproduccions + 1, Recaptacio = Recaptacio + 2 WHERE id = :SongId");
        $updateSongQuery->bindParam("SongId", $SongId, PDO::PARAM_INT);
        $updateSongQuery->execute();
        $a = "L'usuari (" . $username . ") ha incrementat les reproduccions de la cançó (" . $songName . ").";

    }
} catch (PDOException $e) {
    print_r("Connection failed: " . $e->getMessage());
}
echo json_encode($a);
function getUserId()
{
    try {
        $conn = new PDO("mysql:host=localhost;dbname=albums", "root", "super3");
        $query = $conn->prepare("SELECT id FROM usuaris_app ORDER BY RAND() LIMIT 1");
        $query->execute();
        $userResult = $query->fetch(PDO::FETCH_ASSOC);
        return $userResult['id'];
    } catch (PDOException $e) {
        return null;
    }
}

function getSongId()
{
    try {
        $conn = new PDO("mysql:host=localhost;dbname=albums", "root", "super3");
        $query = $conn->prepare("SELECT idSongs FROM songs ORDER BY RAND() LIMIT 1");
        $query->execute();
        $songResult = $query->fetch(PDO::FETCH_ASSOC);
        return $songResult['idSongs'];
    } catch (PDOException $e) {
        return null;
    }
}
